from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chain_pollution.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=False)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    maximlabels, minimlabels, isol = order.calc_extremales(zeta, matrix.rows)
    maxim = []
    for label1 in maximlabels:
        maxim.append(matrix.obj[label1])
    minim = []
    for label2 in minimlabels:
        minim.append(matrix.obj[label2])

    return chain, maxim, minim


def test_chaincandidates(data):
    chain = data[0]
    maxim = data[1]
    minim = data[2]

    candidates, chheight = chain.searchlongchains(maxim, minim)
    # candidates could be an information
    # chheight is an important information
    assert chheight == 4.556
    assert candidates[0][0] == "57"
    assert candidates[0][1] == "30"
    assert candidates[0][2] == 5
    assert candidates[1][0] == "57"
    assert candidates[1][1] == "30"
    assert candidates[1][2] == 5
    assert candidates[2][0] == "57"
    assert candidates[2][1] == "30"
    assert candidates[2][2] == 5
    assert candidates[3][0] == "57"
    assert candidates[3][1] == "30"
    assert candidates[3][2] == 5
    assert candidates[4][0] == "57"
    assert candidates[4][1] == "30"
    assert candidates[4][2] == 5
    assert candidates[5][0] == "48"
    assert candidates[5][1] == "30"
    assert candidates[5][2] == 5


def test_candidatestatistics(data):
    chain = data[0]
    maxim = data[1]
    minim = data[2]

    candidates, chheight = chain.searchlongchains(maxim, minim)
    statchains = chain.search_statistics(maxim, minim, candidates)
    # statchains and chheight are important pieces of information
    assert chheight == 4.556
    assert statchains[0][0] == "18"
    assert statchains[0][1] == "30"
    assert statchains[0][2] == 0
    assert statchains[1][0] == "35"
    assert statchains[1][1] == "30"
    assert statchains[1][2] == 0
    assert statchains[2][0] == "57"
    assert statchains[2][1] == "30"
    assert statchains[2][2] == 5
    assert statchains[3][0] == "48"
    assert statchains[3][1] == "30"
    assert statchains[3][2] == 1
    assert statchains[4][0] == "34"
    assert statchains[4][1] == "30"
    assert statchains[4][2] == 0
