# content of test_class.py
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chain210714_2.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=False)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    return csv, matrix, chain


def test_datamatrix_xrk(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]

    assert matrix.rows == 11
    assert matrix.cols == 2
    rlist = matrix.obj
    assert rlist == ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]
    assert csv.prop == ["q1", "q2"]
    assert csv.obj == ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]

    assert csv.cols == 2
    assert matrix.rows == 11

    # user input
    startvertex = "a"  # Taken from the HD or the context
    sinkvertex = "j"  # Taken from the HD or the context

    startvertexidx = matrix.obj.index(startvertex)
    sinkvertexidx = matrix.obj.index(sinkvertex)
    assert startvertexidx == 0
    assert sinkvertexidx == 9

    graph = chain.generate_dict_of_graph()  # 13.07.2020
    assert graph == {0: [], 1: [0], 2: [0], 3: [1], 4: [3],
                     5: [2], 6: [4, 8], 7: [6], 8: [5],
                     9: [6, 10], 10: [1, 8]}

    paths, flch = chain.connectivity(startvertexidx, sinkvertexidx)
    if flch == 1:
        assert paths == [
            [9, 6, 4, 3, 1, 0],
            [9, 6, 8, 5, 2, 0],
            [9, 10, 1, 0],
            [9, 10, 8, 5, 2, 0],
        ]
    else:
        assert paths == []


def test_chainheight(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]
    assert csv.rows == 11
    assert csv.cols == 2
    maxch = 6  # user input
    startvertex = "a"  # user input
    sinkvertex = "j"  # user input
    # names --> idx
    startvertexidx = matrix.obj.index(startvertex)
    sinkvertexidx = matrix.obj.index(sinkvertex)
    numberofchains, chainlevel, chainsets = chain.chainheight(
        startvertexidx, sinkvertexidx, maxch
    )
    # number of chains (noc) with height maxch
    assert numberofchains == 3
    # chains explicitly given
    assert chainsets[0] == {"g", "e", "b", "a", "j", "d"}
    assert chainsets[1] == {"g", "f", "i", "a", "j", "c"}
    assert chainsets[2] == {"f", "k", "i", "a", "j", "c"}


def test_chainsimilarity(data):
    csv = data[0]
    chain = data[2]
    startvertex = "j"  # user input
    sinkvertex = "a"  # user input
    maxch = 6  # user input
    # names --> idx
    startvertexidx = csv.obj.index(startvertex)
    sinkvertexidx = csv.obj.index(sinkvertex)
    noc, chl, chs = chain.chainheight(startvertexidx, sinkvertexidx, maxch)
    chainm = chain.chainsimilarity(chs, chl)
    assert chainm == [[1.0, 0.333, 0.2],
                      [0.333, 1.0, 0.714],
                      [0.2, 0.714, 1.0]]
