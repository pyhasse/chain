from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = '/data/chain_pollution.csv'
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath+TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    return csv, matrix, chain


def test_datamatrix_xrk(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]

    assert csv.rows == 14
    assert csv.cols == 4
    assert matrix.eqm == [[0], [1], [2], [3], [4], [5],
                          [6], [7], [8], [9], [10], [11], [12], [13]]
    assert matrix.rows == 14
    assert matrix.obj_idx == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    assert csv.prop == ['Pb', 'Cd', 'Zn', 'S']
    assert csv.obj == ['18', '35', '57', '48', '34', '14', '38',
                       '9', '41', '22', '45', '17', '6', '30']
    assert csv.cols == 4

    # user input
    startvertexn = "30"

    # Taken from the HD or the context
    # ...n to indicate 'name'

    sinkvertexn = "57"

    # Taken from the HD or the context
    # ...n to indicate name

    # object name --> objectidx      # new 13.07.2020

    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    # finding the indices in the reduced dm
    paths, flch = chain.connectivity(startvertex, sinkvertex)
    if flch == 1:
        assert paths == [[2, 5, 8, 11, 13],
                         [2, 5, 9, 11, 13],
                         [2, 5, 9, 12, 13],
                         [2, 5, 10, 12, 13],
                         [2, 6, 10, 12, 13]]
    else:
        assert paths == []


def test_chainheight(data):
    matrix = data[1]
    chain = data[2]
    maxch = 5           # user input
    startvertexn = "30"  # user input
    sinkvertexn = "57"   # user input

    # object name --> objectidx
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    noc, chl, chs = chain.chainheight(startvertex,
                                      sinkvertex,
                                      maxch)

    # number of chains (noc) with height maxch
    assert noc == 5


def test_chainsimilarity(data):
    chain = data[2]
    matrix = data[1]
    maxch = 5  # user input
    startvertexn = "30"  # user input
    sinkvertexn = "57"  # user input
    # name --> idx
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)
    noc, chl, chs = chain.chainheight(startvertex,
                                      sinkvertex,
                                      maxch)

    chainm = chain.chainsimilarity(chs, chl)
    assert chainm == [[1.0, 0.667, 0.429, 0.429, 0.25],
                      [0.667, 1.0, 0.667, 0.429, 0.25],
                      [0.429, 0.667, 1.0, 0.667, 0.429],
                      [0.429, 0.429, 0.667, 1.0, 0.667],
                      [0.25, 0.25, 0.429, 0.667, 1.0]]
