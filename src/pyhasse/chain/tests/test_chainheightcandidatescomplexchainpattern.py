from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chaindivlength3.txt"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    maximlabels, minimlabels, isol = order.calc_extremales(zeta, csv.rows)
    maxim = []
    for label1 in maximlabels:
        maxim.append(matrix.objred[label1])
    minim = []
    for label2 in minimlabels:
        minim.append(matrix.objred[label2])

    return csv, matrix, order, zeta, covd, cov, chain, maxim, minim


def test_chaincandidates(data):
    chain = data[6]
    maxim = data[7]
    minim = data[8]

    candidates, chheight = chain.searchlongchains(maxim, minim)
    # candidates could be an information
    # chheight is an important information
    assert chheight == 4.0


def test_candidatestatistics(data):
    chain = data[6]
    maxim = data[7]
    minim = data[8]

    candidates, chheight = chain.searchlongchains(maxim, minim)
    statchains = chain.search_statistics(maxim, minim, candidates)
    # statchains and chheight are important pieces of information
    assert chheight == 4
    assert statchains[0][0] == "d"
    assert statchains[0][1] == "a"
    assert statchains[0][2] == 5


def test_candidatestatisticsoutput(data):
    chain = data[6]
    maxim = data[7]
    minim = data[8]

    candidates, chheight = chain.searchlongchains(maxim, minim)
    statchains = chain.search_statistics(maxim, minim, candidates)
    # statchains and chheight are important pieces of information
    assert chheight == 4.0
    assert statchains == [("d", "a", 5)]
