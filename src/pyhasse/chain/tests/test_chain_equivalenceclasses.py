from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/test25022015.txt"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(csv, matrix, cov)  # Aufrufreihenfolge geaendert
    return (
        csv,
        matrix,
        chain,
        covd,
        cov,
    )


def test_datamatrix_xrk(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]
    assert csv.rows == 10
    assert csv.cols == 2
    assert matrix.rows == 6  # rred
    # assert eqm == [[0, 2, 5], [1], [3, 4], [6, 9], [7], [8]]
    assert matrix.prop == ["q1", "q2"]
    assert csv.obj == ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
    assert matrix.obj == ["a", "b", "d", "g", "h", "i"]

    # user input
    startvertexn = "a"  # Taken from the HD or the context
    sinkvertexn = "g"  # Taken from the HD or the context

    # name --- idx                14.07.2020
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    # finding the indices in the reduced dm
    paths, flch = chain.connectivity(startvertex, sinkvertex)
    if flch == 1:
        assert paths == [[3, 1, 0], [3, 2, 0]]
    else:
        assert paths == []


def test_chainheight(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]

    assert csv.rows == 10
    assert csv.cols == 2
    maxch = 3  # user input
    startvertex = "a"  # user input
    sinkvertex = "g"  # user input
    assert matrix.obj == ["a", "b", "d", "g", "h", "i"]
    # matrix.obj because chain-operations only with representants
    startvertexidx = matrix.obj.index(startvertex)
    sinkvertexidx = matrix.obj.index(sinkvertex)
    numberofchains, chainlevel, chainsets = chain.chainheight(
        startvertexidx, sinkvertexidx, maxch
    )

    # number of chains (noc) with height maxch
    assert numberofchains == 2

    # chains explicitly given
    assert chainsets[0] == {"a", "b", "g"}
    assert chainsets[1] == {"a", "d", "g"}


def test_chainsimilarity(data):
    matrix = data[1]
    chain = data[2]

    maxch = 3  # user input
    startvertex = "a"  # user input
    sinkvertex = "g"  # user input
    startvertexidx = matrix.obj.index(startvertex)
    sinkvertexidx = matrix.obj.index(sinkvertex)
    numberofchains, chainlevel, chainsets = chain.chainheight(
        startvertexidx, sinkvertexidx, maxch
    )
    chainm = chain.chainsimilarity(chainsets, chainlevel)
    assert chainm == [[1.0, 0.5], [0.5, 1.0]]
