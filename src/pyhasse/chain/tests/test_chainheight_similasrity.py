from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chain210714_2.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    return (
        csv,
        matrix,
        chain,
        covd,
        cov,
    )


def test_chainheight(data):
    matrix = data[1]
    chain = data[2]

    startvertex = "a"  # user input
    sinkvertex = "j"  # user input
    maxch = 6  # user input
    startvertexidx = matrix.obj.index(startvertex)
    sinkvertexidx = matrix.obj.index(sinkvertex)
    noc, chl, chs = chain.chainheight(startvertexidx, sinkvertexidx, maxch)
    # noc, chl, chs = chain.chainheight(startvertex,
    #                                   sinkvertex,
    #                                   maxch)
    # number of chains (noc) with height maxch
    assert noc == 3  # 4 chains, but 3 chains with maxch=6

    # chains explicitly given
    assert chs[0] == {"g", "e", "b", "a", "j", "d"}
    assert chs[1] == {"g", "f", "i", "a", "j", "c"}
    assert chs[2] == {"f", "k", "i", "a", "j", "c"}


def test_chainsimilarity(data):
    matrix = data[1]
    chain = data[2]

    startvertexn = "a"  # user input
    sinkvertexn = "j"  # user input
    maxch = 6  # user input

    # name--> labels
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    noc, chl, chs = chain.chainheight(startvertex, sinkvertex, maxch)
    chainm = chain.chainsimilarity(chs, chl)
    assert chainm == [[1.0, 0.333, 0.2],
                      [0.333, 1.0, 0.714],
                      [0.2, 0.714, 1.0]]
