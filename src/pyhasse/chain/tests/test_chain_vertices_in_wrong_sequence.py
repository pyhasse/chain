from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chain210714_2.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    return (
        csv,
        matrix,
        chain,
        covd,
        cov,
    )


def test_connectivitiy(data):
    csv = data[0]
    matrix = data[1]
    chain = data[2]

    assert matrix.rows == 11
    assert matrix.cols == 2
    # assert csv.eqm == [[0], [1], [2], [3], [4],
    #                   [5], [6], [7], [8], [9], [10]]
    assert matrix.rows == 11
    # assert matrix.redobj == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    assert matrix.prop == ["q1", "q2"]
    assert matrix.obj == ["a", "b", "c", "d", "e",
                          "f", "g", "h", "i", "j", "k"]

    assert csv.cols == 2
    assert csv.rows == 11

    # user input
    startvertexn = "j"  # Taken from the HD or the context
    sinkvertexn = "a"  # Taken from the HD or the context

    # names --> idx
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    paths, flch = chain.connectivity(startvertex, sinkvertex)
    assert paths == [
        [9, 6, 4, 3, 1, 0],
        [9, 6, 8, 5, 2, 0],
        [9, 10, 1, 0],
        [9, 10, 8, 5, 2, 0],
    ]


def test_chainheight(data):
    matrix = data[1]
    chain = data[2]

    # user input
    startvertexn = "j"  # Taken from the HD or the context
    sinkvertexn = "a"  # Taken from the HD or the context
    maxch = 6

    # names --> idx
    startvertex = matrix.obj.index(startvertexn)
    sinkvertex = matrix.obj.index(sinkvertexn)

    noc, chl, chs = chain.chainheight(startvertex, sinkvertex, maxch)
    # number of chains (noc) with height maxch
    assert noc == 3

    # chains explicitly given
    assert chs[0] == {"g", "e", "b", "a", "j", "d"}
    assert chs[1] == {"g", "f", "i", "a", "j", "c"}
    assert chs[2] == {"f", "k", "i", "a", "j", "c"}
