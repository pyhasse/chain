# content of test_class.py
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
import os
import pytest


@pytest.fixture
def data():

    TESTFILENAME = "/data/chain210714_2.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    return csv, matrix, order, zeta, chain, covd, cov


def test_prepare_calculations(data):
    chain = data[4]

    # user input
    startvertex = "a"
    sinkvertex = "j"
    maxch = 6
    # calculation istart , isink should be taken from representantslist. Here:
    # no problem, because no nontrivial equivalence class exists
    path, flch = chain.connectivity(
        startvertex, sinkvertex
    )
    # 14.07.2020 argument list changed
    if flch == 1:
        noc, chl, chs = chain.chainheight(startvertex, sinkvertex, maxch)
        chainm = chain.chainsimilarity(chs, chl)
        noc, name_ord, vcm, chlist = chain.chain_graphics(noc, chainm)
        assert chainm == [[1.0, 0.333, 0.2],
                          [0.333, 1.0, 0.714],
                          [0.2, 0.714, 1.0]]

        cbar = []
        for i in range(0, len(chainm)):
            for j in range(i + 1, len(chainm)):
                cbar.append(chainm[i][j])

        assert len(cbar) == len(chlist)

        tmpl = '{{"legendLabel": "{0}", "magnitude": {1} }},\n'
        data = []
        counter = 0

        for i in chlist:
            pair = "(" + str(i[0] + 1) + "," + str(i[1] + 1) + ")"
            data += tmpl.format(pair, vcm[counter])
            counter += 1

        data += "];"
