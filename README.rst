=====================
Module: pyhasse.chain
=====================

* Free software: MIT license
* Documentation: https://pyhasse.org


Features
--------

Calculate:

- Identify the set of mutually comparable objects: a chain
- Typical results: list of objects of a chain, the length and...
- ...number of chains between a starting and an end point
