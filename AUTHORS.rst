=======
Credits
=======

Development Lead
----------------

* Rainer Bruggemann <rainer.bruggemann@pyhasse.org>

Contributors
------------

* Peter Koppatz <peter.koppatz@pyhasse.org>
* Valentin Pratz <vp@vocaword.org>



